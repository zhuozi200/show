window.onload = function() {
	var tab = document.querySelector(".tabs").children;
	var showBar = document.querySelectorAll(".show-bar");
	var playBtn = document.querySelector(".play-btn");
	var video = document.querySelector("video");
	var black = document.querySelector(".black");

	for(var i = 0; i < tab.length; i++) {
		tab[i].onclick = function() {
			for(var j = 0; j < tab.length; j++) {
				tab[j].classList.remove("on");
			}
			this.classList.add("on");
		}
	}
	// 添加切换显示板块事件
	tab[0].addEventListener("click", function() {
		showBar[0].style.display = "block";
		showBar[1].style.display = "none";
	});
	tab[1].addEventListener("click", function() {
		showBar[0].style.display = "none";
		showBar[1].style.display = "block";
	});
	
	// 播放按钮
	playBtn.onclick = function() {
		video.play();
		black.style.display = "none";
		playBtn.style.display = "none";
		video.controls = "controls";
	}
	// 暂停样式
	video.onclick = function() {
		if(this.paused == false) {
			video.pause();
// 			if(video.paused == true) {
// 				video.pause();
// 			}
			black.style.display = "block";
			playBtn.style.display = "flex";
			setTimeout(function() {
				video.removeAttribute("controls");
			}, 10);
		}
	}
}

var vm = new Vue({
	el: ".news-block",
	data: {
		msg: [{
			imgSrc: "https://pic.downk.cc/item/5e7208bbe83c3a1e3af9e5c5.png",
			imgAlt: "新闻1",
			url: "pages/news1.html",
			h3: "人工智能技术布局最多的国家，中国",
			para: "“中国和美国是AI专利的主要布局区域，中国已经成为被人工智能技术布局最多的国家。”…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208bbe83c3a1e3af9e5c7.png",
			imgAlt: "新闻2",
			url: "pages/news2.html",
			h3: "2019第四届全球人工智能峰会圆满举办",
			para: "今年的峰会以“AI助力产业升级” 为主题，由聚想会主办上海博蔚会展有限公司承办，CFAI2019得到了多家全球及中国人工智能产业…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208bbe83c3a1e3af9e5c9.png",
			imgAlt: "新闻3",
			url: "pages/news3.html",
			h3: "中国新一代人工智能发展报告发布：中国AI论文数量全球第一",
			para: "IT之家5月25日消息 昨日，科技部新一代人工智能发展研究中心、中国科学技术发展战略研究院联合国内外十余家机构…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208bbe83c3a1e3af9e5cb.png",
			imgAlt: "新闻4",
			url: "pages/news4.html",
			h3: "人工智能专利申请全球领先，中国AI力量正在崛起",
			para: "如今，人工智能作为当下最热门的新科技领域，全球各个国家的科技先锋都投入了大量的资金、人力进行技术研发…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208bbe83c3a1e3af9e5cd.png",
			imgAlt: "新闻5",
			url: "pages/news5.html",
			h3: "骨科机器人实现远程手术 辛国斌：中国医学装备产业已走向高质量发展阶段",
			para: "近年来，中国医学装备人工智能实现快速发展，医学装备产业已走向高质量发展阶段…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208dbe83c3a1e3af9fa11.png",
			imgAlt: "新闻6",
			url: "pages/news6.html",
			h3: "在大数据以及人工智能两个领域 中国脚步飞快",
			para: "台湾媒体报道称，大陆在大数据及人工智能（AI）积极推动产业化之际，预估2019年大陆大数据核心产业规模有望突破7200亿元人民币…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208dbe83c3a1e3af9fa08.png",
			imgAlt: "新闻7",
			url: "https://baijiahao.baidu.com/s?id=1634342011842885683&wfr=spider&for=pc",
			h3: "耶鲁大学温德尔·瓦拉赫：中国人工智能伦理建设正经历飞速发展阶段",
			para: "瓦拉赫参加了太和智库主办的中国人工智能伦理与治理网络构建研讨会。尽管对中国情况了解不多，但他已经能明显感觉到这里的发展氛围…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208dbe83c3a1e3af9fa0a.png",
			imgAlt: "新闻8",
			url: "pages/news8.html",
			h3: "中国发展人工智能优势何在？业内大咖谈发展机遇与挑战",
			para: "5月6日晚，在第二届数字中国建设峰会的“数字经济·闽江夜话”活动上，各界代表齐聚一堂，深入探讨我国人工智能产业的发展机遇与挑战…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208dbe83c3a1e3af9fa0c.png",
			imgAlt: "新闻9",
			url: "#",
			h3: "厉害了我的国，人工智能领域传来好消息：中国实现弯道超车",
			para: "一直以来，在人工智能技术方面，美国都是一家独大，但是这些年，中国一直都在努力发展探索研究，花费了大量的心血在这一技术研发上…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208dbe83c3a1e3af9fa0e.png",
			imgAlt: "新闻10",
			url: "p#",
			h3: "为啥中国的人工智能这么强？美媒来华后恍然大悟",
			para: "美媒注意到，中国人工智能产业的飞速发展，催生了很多从未有过的职业。过去三年，中国的数据标注产业规模已经增长了10倍…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208e2e83c3a1e3af9ff14.png",
			imgAlt: "新闻11",
			url: "#",
			h3: "一座城的智能发展：中国人工智能城市产业联盟",
			para: "人工智能的推进速度越来越快，近日在厦门开始倡议打造中国人工智能城市产业联盟。 这个事情充分体现全国各地逐步开始推行和倡议人工智能这件事…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208e2e83c3a1e3af9ff16.png",
			imgAlt: "新闻12",
			url: "#",
			h3: "工信部发布中国互联网百强榜单 科大讯飞等24家人工智能企业上榜",
			para: "中国互联网协会和工业和信息化部信息中心联合发布了2019年中国互联网企业100强榜单。以科大讯飞为代表的人工智能企业迅速崛起，上榜数高达到24家…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208e2e83c3a1e3af9ff0d.png",
			imgAlt: "新闻13",
			url: "#",
			h3: "上海已拥有人工智能核心企业1000余家 初步建成中国人工智能发展领先地区",
			para: "上海初步建成为中国人工智能发展的领先地区之一。据有关方面统计，上海已拥有人工智能核心企业1000余家，居全国前列…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208e2e83c3a1e3af9ff10.png",
			imgAlt: "新闻14",
			url: "#",
			h3: "向隽：中国发展人工智能具有先天优势",
			para: "当前,人工智能正在全球范围内蓬勃兴起,作为引领未来的战略性技术,对推进经济发展、维护国家安全、改善人民生活具有重要意义…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208e2e83c3a1e3af9ff12.png",
			imgAlt: "新闻15",
			url: "#",
			h3: "王志刚：中国人工智能发展走在世界第一方阵",
			para: "王志刚表示，中国的人工智能发展在世界上走在第一方阵，“国家政府出台了人工智能发展战略，企业和科研院所都把人工智能作为重点。” …",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208f2e83c3a1e3afa07da.png",
			imgAlt: "新闻16",
			url: "#",
			h3: "最具挑战的顶级人工智能竞赛在美举办 ——中国团队中科智云斩获第二名",
			para: "在DAVIS2019无监督多目标视频分割挑战赛中，冠军由德国亚琛工业大学的计算机视觉团队获得，来自中国的团队仅以毫厘之差屈居亚军…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208f2e83c3a1e3afa07dc.png",
			imgAlt: "新闻17",
			url: "#",
			h3: "华为发布智慧车载解决方案HiCar生态白皮书！",
			para: "2019华为开发者大会（HDC 2019）期间，华为正式发布了HUAWEI HiCar生态白皮书…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208f2e83c3a1e3afa07df.png",
			imgAlt: "新闻18",
			url: "#",
			h3: "中国工程院院士谭建荣：产品数字孪生与汽车智能制造关键技术趋势",
			para: "在本次智能汽车高峰论坛上，中国工程院院士 、浙江大学教授谭建荣带来了《产品数字孪生与汽车智能制造——关键技术与发展趋势》的主题演讲…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208f2e83c3a1e3afa07e1.png",
			imgAlt: "新闻19",
			url: "#",
			h3: "中科新松许小刚：智能协作机器人是中国机器人产业发展新节点",
			para: "“协作机器人的未来将围绕负载比重、灵活性等多个方面升级和变革，随着AI技术的快速发展，未来的协作机器人市场将会有很好的发展前景。”…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208f2e83c3a1e3afa07d7.png",
			imgAlt: "新闻20",
			url: "#",
			h3: "人工智能来了！中国工业机器人消费连续6年第一",
			para: "去年一年我国机器人与智能制造行业主营业务收入达到了2862.96亿元，同比增长6.68%。中国工业机器人消费已连续6年稳居全球第一…",
			time: "2019-07-27"
		}]
	}
});

// 翻页模块
var curPage = null;
var pageBtns = document.querySelector(".other-page").querySelectorAll(".btn");
var newsItems = document.querySelectorAll(".news-item");
// 初始化第一页显示
for(var i = 7; i < newsItems.length; i++) {
	newsItems[i].style.display = "none";
}
// 选页
for(var i = 1; i < pageBtns.length - 1; i++) {
	pageBtns[i].index = i;
	pageBtns[i].addEventListener("click", function() {
		curPage = this.index - 1;
		render();
	}); 
}
// 翻页
pageBtns[0].addEventListener("click", function() { // 上一页
	if(curPage > 0) {
		curPage--;
	} else {
		alert("已经是第一页啦");
	}
	render();
});
pageBtns[pageBtns.length - 1].addEventListener("click", function() { // 下一页
	if(curPage < pageBtns.length - 3) {
		curPage++;
	} else {
		alert("已经是最后一页啦");
	}
	render();
});

// 按钮变色
for(var i = 1; i < pageBtns.length - 1; i++) {
	pageBtns[i].addEventListener("click", function() {
		for(var j = 0; j < pageBtns.length; j++) {
			pageBtns[j].style.backgroundColor = "#a5bbd4";
		}
		this.style.backgroundColor = "#546ad9";
	}); 
}

// 渲染内容
function render() {
	for(var i = 0; i < newsItems.length; i++) {
		newsItems[i].style.display = "none";
	}
	for(var j = curPage * 7; j < (curPage+1) * 7 && j < newsItems.length; j++) {
		newsItems[j].style.display = "block";
	}
	// 改变按钮颜色
	for(var j = 0; j < pageBtns.length; j++) {
		pageBtns[j].style.backgroundColor = "#a5bbd4";
	}
	pageBtns[curPage + 1].style.backgroundColor = "#546ad9";
}
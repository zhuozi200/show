window.addEventListener("load", function() {
	var dotsArr = document.querySelector(".dots").children;
	var content = document.querySelector(".content");
	var h3Arr = content.querySelectorAll("h3");
	var para = content.querySelector("p");
	var curUrl = content.querySelector("a");
	var slideImgArr = document.querySelectorAll(".img");
	var prevIndex = 0;
	console.log(slideImgArr);
	var infoArr = [
		{name: "吴恩达：", hitokoto: "我眼中的人工智能", intro: "“由于云或移动领域已经有规模较大的公司了，现在该领域很难出现新的产业。因此，人工智能领域的创业空间比移动或云领域的机会大……”", url: "pages/page9.html"},
		{name: "刘庆峰：", hitokoto: "人工智能既是机遇也是挑战", intro: "“计算机早就轻而易举地能做到普通人不可能完成的计算，这是计算智能。再往后叫感知智能，真正代表未来是认知智能，就是让机器具备学习、理解、推理和决策的能力……”", url: "pages/page10.html"},
		{name: "贾扬清：", hitokoto: "我对人工智能方向的一点浅见", intro: "“AI 这个方向会怎么走？我个人的感觉，虽然大家一直关注AI框架，但是作为 AI 工程师，我们应该跳出框架的桎梏，往更广泛的领域寻找价值。”", url: "pages/page11.html"},
		{name: "陈云霁：", hitokoto: "寻找人工智能新爆点", intro: "“我确实没有预见到人工智能时代来得这么快。但是有些事情，一定是在别人都不太看好的情况下去做，下一番苦功夫，才有机会领先。”", url: "pages/page12.html"},
		{name: "周志华：", hitokoto: "AI产业将更凸显个人英雄主义", intro: "“从技术上看，神经网络其实是个简单的数学函数，通过迭代嵌套得出的系统。在一些图像视频处理任务中，深度神经网络有时候并不是最佳的选择。”", url: "pages/page13.html"}
	];
	for(var i = 0; i < dotsArr.length; i++) {
		dotsArr[i].index = i;  //保存数组下标
	}
	for(var i = 0; i < dotsArr.length; i++) {
		dotsArr[i].addEventListener("click", function() {
			for(var j = 0; j < dotsArr.length; j++) {
				dotsArr[j].classList.remove("focus");
			}
			this.classList.add("focus");
			//更换文字
			h3Arr[0].innerHTML = infoArr[this.index].name;
			h3Arr[1].innerHTML = infoArr[this.index].hitokoto;
			para.innerHTML = infoArr[this.index].intro;
			curUrl.href = infoArr[this.index].url;
			
			//让当前图片的z-index最高
			for(var j = 0; j < dotsArr.length; j++) {
				slideImgArr[j].style.zIndex = 1;
			}
			slideImgArr[this.index].style.zIndex = 5;
			
			//更换图片
			slideImgArr[this.index].classList.add("cur-img");
			var that = this;
			setTimeout(function() {
				removeClass(that);
			}, 500);
		});
	}
	function removeClass(obj) {
		console.log("本次点击的数组下标是：",obj.index)
		console.log("上一次点击的数组下标是：", prevIndex)
		console.log(obj)
		if(prevIndex != obj.index) {
			slideImgArr[prevIndex].classList.remove("cur-img");
		}
		prevIndex = obj.index;
	}
});
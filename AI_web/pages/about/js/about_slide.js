window.onload = function() {
	var trans = document.querySelector(".transparent");
	var slides = document.querySelector(".slides");
	var sliArr = slides.querySelectorAll("figure");
	var dotsArr = document.querySelector(".focus").children;
	
	// 给轮播图外层盒子设置高度
	var leaderHeight = document.querySelector(".slide3").offsetHeight; //获取轮播子项最高高度
	slides.style.height = leaderHeight + "px";
	
	var sliClass = ["slide1", "slide2", "slide3", "slide4", "slide5"];
	var index = 3;
	var interval = setInterval(slidePlay, 4000);
	
	// 鼠标悬浮停止轮播
	trans.addEventListener("mouseover", function() {
		clearInterval(interval);
	});
	trans.addEventListener("mouseout", function() {
		interval = setInterval(slidePlay, 4000);
	});
	
	function slidePlay() {
		sliClass.unshift(sliClass[sliClass.length - 1]);
		sliClass.pop(sliClass[sliClass.length - 1]);
		for(var i = 0; i < sliArr.length; i++) {
			sliArr[i].className = sliClass[i];
		}
		// 轮播焦点
		for(var j = 0; j < dotsArr.length; j++) {
			dotsArr[j].classList.remove("on");
		}
		dotsArr[index].classList.add("on");
		index++;
		if(index >= dotsArr.length) {
			index = 0;
		}
	}
}
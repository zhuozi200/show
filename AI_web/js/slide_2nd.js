var info = document.querySelector(".info");
var para = info.getElementsByTagName("p")[0];
var btn = document.getElementsByTagName("button")[0];
var num = document.querySelector(".num");
var title = document.querySelector(".underline");
var topEle = document.querySelector(".topEle");
var bottomEle = document.querySelector(".bottomEle");

//新建数组存储标题
var aTit = ["中国的语音识别技术", "中国的AI芯片技术", "中国的人工智能应用", "中国的AI学术成就", "AI促进我国经济发展"];

//新建数组存储正文内容
var aTxt = [
	"中国在自然语言处理特别是语音识别领域已经达到国际先进水平。2015年中国智能语音产业规模达到40.3亿元,较2014年增长41.0%,远高于全球语音产业增长速度，科大讯飞已跻身全球排名前五,占有中文语音技术市场70%以上市场份额,语音合成产品市场份额达到70%以上。预计到2020年,中国语音产业规模预计达到100.7亿元。",
	"这几年，中国在AI芯片技术成就上同样取得了非常突出的成就。眼擎科技公司就发布了全球首个AI视觉成像芯片。在人工智能芯片领域上，中科院寒武纪科技公司深度学习处理器的能效比主流CPU、GPU有两个数量级的提升。另外一个对于中国人工智能具有重要标志性意义的是华为的“麒麟970”，它是全球第一款集成了NPU神经网络单元具备人工智能(AI)能力的手机芯片，抢在了高通和苹果之前。",
	"在中国，有大量的AI应用于涌现，医疗、健康、物流、安防等各个领域，且取得显著效果。医疗领域，以肺结核为例，人工诊疗准确率通常约为70%，而国防科技大学开发的一套智能辅诊系统能达到90%以上。物流领域，智能仓储物流解决方案的发展，促进装卸搬运、分拣包装、加工配送等物流环节的智能化发展。腾讯云优图天眼系统，利用AI人脸检索技术，携手福建省公安厅在寻回124名走失人员。", 
	"这几年，华人学者在人工智能学术上的成就，有目共睹。2011年到2015年，中国学者在AI领域出版的论文数量排名世界第一，创下了超过4.1万个出版物的记录。中国人工智能专利申请数累计超过1.57万项，位列全球第二。到2018年，中国在人工智能方面的人才拥有量上排名全球第二；在全球人工智能专利布局上中国已超美国和日本，成为全球人工智能行业专利最多的国家。", 
	"人工智能市场前景巨大，随着人工智能技术的逐渐成熟，科技、制造业等业界巨头布局的深入，应用场景不断扩展。2018年中国人工智能市场规模约为238.2亿元，增长率达到56.6%。预计2019年，中国人工智能市场规模将近280亿元。在风险投资上，中国人工智能领域的投融资占到了全球的60%，成为全球最“吸金”的国家。"
];
var order = 0;
//初始化界面文字内容
window.addEventListener("load", function() {
	title.innerHTML = aTit[0];
	para.innerHTML = aTxt[0];
});

//切换先消除原先的内联样式
function removeAnime() {
	topEle.classList.remove("eventTop");
	bottomEle.classList.remove("eventBottom");
	// console.log(topEle.classList);
	// console.log(bottomEle.classList);
}

//添加内联样式动画
function flashAnime() {
	order++;
	(order > 4) && (order = 0);
	num.innerHTML = "0" + (order + 1);
	typewriter(title, aTit[order]);
	para.innerHTML = aTxt[order];
	topEle.className += " eventTop";
	bottomEle.className += " eventBottom";
}

//打字机动画效果函数
function typewriter(obj, str) {
	var i = 0;
	var interval = 
	setInterval(function() {
		var subStr = str.substring(0, i);
		obj.innerHTML = subStr;
		if(i < str.length) {
			i++;
		} else {
			clearInterval(interval);
		}
	},100);
}

//轮播焦点部分
var aList = document.querySelector(".dots").children[0].children;
var j = 0;
function focus() {
	aList[j].className = "";
	j++;
	if(j >= aList.length) j = 0;
	aList[j].className = "active";
}

//轮播图脚本
var aImg = document.querySelector(".slide2").children;
console.log(aImg);
var aLiClass = ["sli1", "sli2", "sli3", "sli4", "sli5"];
var lastIndex = aLiClass.length - 1;
function nextPage() {
	aLiClass.unshift(aLiClass[lastIndex]);
	aLiClass.pop();
	for(var i = 0; i < aImg.length; i++) {
		aImg[i].setAttribute("class", aLiClass[i]);
	}
}

//轮播图播放的总函数
function playFun() {
	removeAnime();
	setTimeout(flashAnime, 30);
	nextPage();
	focus();
}

var play = setInterval(playFun, 3600);

//鼠标悬停停止轮播
var info = document.querySelector(".info");
for(var i = 0; i < aImg.length; i++) {
	aImg[i].addEventListener("mouseover", function(e) {
		clearInterval(play);
	});
	aImg[i].addEventListener("mouseout", function(e) {
		play = setInterval(playFun, 3600);
	});
}
info.addEventListener("mouseover", function(e) {
	clearInterval(play);
	removeAnime();
	setTimeout(function() {
		topEle.className += " eventTop";
		bottomEle.className += " eventBottom";
	}, 30);
});
info.addEventListener("mouseout", function(e) {
	play = setInterval(playFun, 3600);
});
var leftBtn = document.querySelector(".glyphicon-menu-left");
var rightBtn = document.querySelector(".glyphicon-menu-right");
var imgGroup = document.querySelector(".img-group");
var moveLength = getStyle(imgGroup, "width") / imgGroup.children.length;
var boxWidth = getStyle(document.querySelector(".slide-box"), "width");
var maxMove = getStyle(imgGroup, "width") - boxWidth;
window.resize = function() {
	maxMove = getStyle(imgGroup, "width") - boxWidth;
}
var curMove = 0;
leftBtn.addEventListener("click", function() {
	if(curMove < 0) {
		curMove += moveLength;
		imgGroup.style.transform = `translateX(${curMove}px)`;
	}
});
rightBtn.addEventListener("click", function() {
	if(curMove > -maxMove) {
		curMove -= moveLength;
		imgGroup.style.transform = `translateX(${curMove}px)`;
	}
});

function getStyle(obj, attr) {
	if(window.getComputedStyle) {
		return parseInt(window.getComputedStyle(obj, null)[attr]);
	} else {
		return parseInt(obj.currentStyle[attr]);
	}
}

// 获取日期
var today = new Date();
var localeDate = today.toLocaleDateString().replace(/\//g, "-");
var timeArr = document.querySelectorAll("time");
for(var i = 0; i < timeArr.length; i++) {
	timeArr[i].innerHTML = localeDate;
}
localeDate = today.toLocaleDateString().replace(/\//, "年").replace(/\//, "月") + "日";
timeArr[0].innerHTML = localeDate;

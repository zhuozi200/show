window.addEventListener("load", function() {
	var aLi = document.getElementsByClassName("focus")[0].children;
	for(var i = 0; i < aLi.length; i++) {
		aLi[i].index = i;
		aLi[i].addEventListener("click", function() {
			for(var j = 0; j < aLi.length; j++) {
				aLi[j].classList.remove("active");
			}
			this.classList.add("active");
		});
	}
	// 焦点自动轮播
	var i = 1;
	setInterval(function() {
		for(var j = 0; j < aLi.length; j++) {
			aLi[j].classList.remove("active");
		}
		aLi[i].classList.add("active");
		i++;
		if(i >= aLi.length) {
			i = 0;
		}
	}, 4000);
});

// 数据
var vm = new Vue({
	el: '#vueRender',
	data: {
		message: [
			// 发展趋势data
			{
				title: "China's New Trends of AI Industry",
				content: "At present, artificial intelligence has gradually moved from technology to application and has achieved rapid development. However, the current artificial intelligence is still weak artificial intelligence based on specific application areas. It has been shown that most technologies take more than five years to mature. Therefore, there are still many innovations in the field of artificial intelligence, including technological breakthroughs, new application scenarios, etc. and will continue to grow along with a number of innovative and entrepreneurial companies, and become a leading enterprise in a certain field.",
				url: "pages/page1.html"
			},
			{
				title: "Where's the future of Chinese AI?",
				content: "The 4th Global Summit of Artificial Intelligence and Robots  was officially held in Shenzhen. The Summit is sponsored by the Chinese Computer Society , hosted by Lei Feng Net, the Chinese University of Hong Kong , and co-sponsored by the Shenzhen Institute of Artificial Intelligence and Robots. It has received strong guidance from the Shenzhen Municipal Government. It is a top exchange and Exposition in three fields: artificial intelligence and robotics academia, industry and investment. It aims to build a strong cross-border communication and cooperation platform in the field of artificial intelligence in China.",
				url: "pages/page2.html"
			},
			{
				title: "China has the most AI Layout",
				content: "\"China and the United States are the main distribution areas of AI patents, and China has become the country with the largest distribution of AI technology.\" On July 6, at the Engineering Science and Technology Subversive Technology Forum held in Chengdu, Lu Yueguang, academician of the Chinese Academy of Engineering, in his report \"Preliminary Exploration of Subversive Technology Development in the Field of Information and Electronic Engineering\", said after analyzing the indicators such as the proportion of patents between China and the United States, international patents, the degree of mutual concern between the two countries and the hotspots of patent concern.",
				url: "pages/page3.html"
			}
		],
		curTitle: '',
		curContent: '',
		curUrl: '',
		curIndex: 1
	},
	methods: {
		item(e) {
			var curIndex = e.target.index;
			this.typewriterTitle(this.message[curIndex].title);
			this.curContent = this.message[curIndex].content;
			this.curUrl = this.message[curIndex].url;
		},
		typewriterTitle(str) {
			var that = this;
			var i = 0;
			var interval = setInterval(function() {
				curStr = str.substring(0, i);
				that.curTitle = curStr;
				i++;
				if(i > str.length) {
					clearInterval(interval);
				}
			}, 80);
		}
	},
	mounted() {
		this.curTitle = this.message[0].title;
		this.curContent = this.message[0].content;
		this.curUrl = this.message[0].url;
		this.typewriterTitle(this.curTitle);
		var that = this;
		setInterval(function() {
			console.log(that.curIndex);
			that.curTitle = that.message[that.curIndex].title;
			that.curContent = that.message[that.curIndex].content;
			that.curUrl = that.message[that.curIndex].url;
			that.typewriterTitle(that.curTitle);
			that.curIndex++;
			if(that.curIndex >= that.message.length) {
				that.curIndex = 0;
			}
		}, 4000);
	}
});

var info = document.querySelector(".info");
var para = info.getElementsByTagName("p")[0];
var btn = document.getElementsByTagName("button")[0];
var num = document.querySelector(".num");
var title = document.querySelector(".underline");
var topEle = document.querySelector(".topEle");
var bottomEle = document.querySelector(".bottomEle");
console.log("检查");
console.log(info);
console.log(para);
console.log(btn);
console.log(num);
console.log(title);
console.log(topEle);
console.log(bottomEle);

//新建数组存储标题
var aTit = ["Speech Recognition", "AI Chip in China", "AI Application in China", "AI Academy in China", "Economic Development"];

//新建数组存储正文内容
var aTxt = [
	"China has reached an advanced international level in natural language processing, especially in speech recognition. In 2015, the scale of China's intelligent voice industry reached 4.03 billion yuan, which is far higher than the growth rate of the global voice industry. It is estimated that by 2020, the scale of China's voice industry will reach 10.07 billion yuan.",
	"In recent years, China has also made remarkable achievements in AI chip technology. Eye Technologies has released the world's first AI visual imaging chip. Kirin 970, which is an important symbol of AI in China, is the first mobile phone chip in the world that integrates NPU neural network units and has the ability of artificial intelligence (AI). It is ahead of Qualcomm and Apple.",
	"AI application has achieved remarkable results in China. In the medical field, a set of intelligent auxiliary diagnosis system developed by National University of Defense Science and Technology can reach more than 90%. In the field of logistics, the development of intelligent warehousing logistics solutions promotes the intelligent development of logistics links such as handling and handling. ",
	"The number of papers published by Chinese scholars in the field of AI ranks first in the world, setting a record of more than 41,000 publications. China's total number of AI patent applications exceeds 157,000, ranking second in the world. In the global distribution of AI patents, China has surpassed the United States and Japan, and become the country with the largest number of AI patents in the world.",
	"With the gradual maturity of AI technology and the deepening of the layout of giants in science and technology, manufacturing industry and other industries, the application scenarios continue to expand. In 2018, the scale of China's AI market was about 23.82 billion yuan, with a growth rate of 56.6%. It is estimated that in 2019, the scale of China's artificial intelligence market will be nearly 28 billion yuan."
];
var order = 0;
//初始化界面文字内容
window.addEventListener("load", function() {
	title.innerHTML = aTit[0];
	para.innerHTML = aTxt[0];
});

//切换先消除原先的内联样式
function removeAnime() {
	topEle.classList.remove("eventTop");
	bottomEle.classList.remove("eventBottom");
	// console.log(topEle.classList);
	// console.log(bottomEle.classList);
}

//添加内联样式动画
function flashAnime() {
	order++;
	(order > 4) && (order = 0);
	num.innerHTML = "0" + (order + 1);
	typewriter(title, aTit[order]);
	para.innerHTML = aTxt[order];
	topEle.className += " eventTop";
	bottomEle.className += " eventBottom";
}

//打字机动画效果函数
function typewriter(obj, str) {
	var i = 0;
	var interval = 
	setInterval(function() {
		var subStr = str.substring(0, i);
		obj.innerHTML = subStr;
		if(i < str.length) {
			i++;
		} else {
			clearInterval(interval);
		}
	},100);
}

//轮播焦点部分
var aList = document.querySelector(".dots").children[0].children;
var j = 0;
function focus() {
	aList[j].className = "";
	j++;
	if(j >= aList.length) j = 0;
	aList[j].className = "active";
}

//轮播图脚本
var aImg = document.querySelector(".slide2").children;
console.log(aImg);
var aLiClass = ["sli1", "sli2", "sli3", "sli4", "sli5"];
var lastIndex = aLiClass.length - 1;
function nextPage() {
	aLiClass.unshift(aLiClass[lastIndex]);
	aLiClass.pop();
	for(var i = 0; i < aImg.length; i++) {
		aImg[i].setAttribute("class", aLiClass[i]);
	}
}

//轮播图播放的总函数
function playFun() {
	removeAnime();
	setTimeout(flashAnime, 30);
	nextPage();
	focus();
}

var play = setInterval(playFun, 3600);

//鼠标悬停停止轮播
var info = document.querySelector(".info");
for(var i = 0; i < aImg.length; i++) {
	aImg[i].addEventListener("mouseover", function(e) {
		clearInterval(play);
	});
	aImg[i].addEventListener("mouseout", function(e) {
		play = setInterval(playFun, 3600);
	});
}
info.addEventListener("mouseover", function(e) {
	clearInterval(play);
	removeAnime();
	setTimeout(function() {
		topEle.className += " eventTop";
		bottomEle.className += " eventBottom";
	}, 30);
});
info.addEventListener("mouseout", function(e) {
	play = setInterval(playFun, 3600);
});
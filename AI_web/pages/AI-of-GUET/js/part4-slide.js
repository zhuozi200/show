window.addEventListener("load", function() {
	var classArr = ["slide-item1", "slide-item2", "slide-item3", "slide-item4", "slide-item5", "slide-item6", "slide-item7"];
	var mainSlide = document.querySelector(".main-slide");
	var imgArr = mainSlide.querySelectorAll('.slide-item');
	
	changeClass();
	setInterval(changeClass, 5000);
	
	function changeClass() {
		classArr.unshift(classArr[classArr.length - 1]);
		classArr.pop();
		for(var i = 0; i < imgArr.length; i++) {
			imgArr[i].className = "slide-item " + classArr[i];
		}
	}
});
window.onload = function() {
	var tab = document.querySelector(".tabs").children;
	var showBar = document.querySelectorAll(".show-bar");
	var playBtn = document.querySelector(".play-btn");
	var video = document.querySelector("video");
	var black = document.querySelector(".black");

	for(var i = 0; i < tab.length; i++) {
		tab[i].onclick = function() {
			for(var j = 0; j < tab.length; j++) {
				tab[j].classList.remove("on");
			}
			this.classList.add("on");
		}
	}
	// 添加切换显示板块事件
	tab[0].addEventListener("click", function() {
		showBar[0].style.display = "block";
		showBar[1].style.display = "none";
	});
	tab[1].addEventListener("click", function() {
		showBar[0].style.display = "none";
		showBar[1].style.display = "block";
	});
	
	// 播放按钮
	playBtn.onclick = function() {
		video.play();
		black.style.display = "none";
		playBtn.style.display = "none";
		video.controls = "controls";
	}
	// 暂停样式
	video.onclick = function() {
		if(this.paused == false) {
			video.pause();
// 			if(video.paused == true) {
// 				video.pause();
// 			}
			black.style.display = "block";
			playBtn.style.display = "flex";
			setTimeout(function() {
				video.removeAttribute("controls");
			}, 10);
		}
	}
}

var vm = new Vue({
	el: ".news-block",
	data: {
		msg: [{
			imgSrc: "https://pic.downk.cc/item/5e7208bbe83c3a1e3af9e5c5.png",
			imgAlt: "新闻1",
			url: "pages/new1-en.html",
			h3: "The most out-of-art world of AI, China",
			para: "“China and USA are the main layout areas of AI patents,China has become the most widely distributed”…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208bbe83c3a1e3af9e5c7.png",
			imgAlt: "新闻2",
			url: "pages/new2-en.html",
			h3: "2019 fourth Global AI Summit successfully",
			para: "this year summit will be hosted by Shanghai Bowei Convention and Exhibition Co., Ltd., with the theme of…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208bbe83c3a1e3af9e5c9.png",
			imgAlt: "新闻3",
			url: "pages/new3-en.html",
			h3: "China new generation of AI development report: the number of AI",
			para: "May 25, yesterday, the New Generation artificial Intelligence Development Research...",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208bbe83c3a1e3af9e5cb.png",
			imgAlt: "新闻4",
			url: "pages/new4-en.html",
			h3: "AI is the worldundefineds leading, and Chinaundefineds AI power rising.",
			para: "Nowadays, artificial intelligence, as the most popular new technology field, has invested a lot of money…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208bbe83c3a1e3af9e5cd.png",
			imgAlt: "新闻5",
			url: "pages/new5-en.html",
			h3: "Medical Equipment Industry in China has gone high-quality stage of development",
			para: "In recent years, the artificial intelligence of medical equipment in China has developed rapidly…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208dbe83c3a1e3af9fa11.png",
			imgAlt: "新闻6",
			url: "pages/new6-en.html",
			h3: "in both that big data and the AI, Chinaundefineds pace is fast",
			para: "Taiwan media reported that while big data and artificial intelligence (AI) actively promoted industrialization…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208dbe83c3a1e3af9fa08.png",
			imgAlt: "新闻7",
			url: "https://baijiahao.baidu.com/s?id=1634342011842885683&wfr=spider&for=pc",
			h3: "Wendell Warah, Yale University: The Construction of AI Ethics in China",
			para: "J. Wallage attended a seminar on the construction of Chinese artificial intelligence ethics…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208dbe83c3a1e3af9fa0a.png",
			imgAlt: "新闻8",
			url: "pages/news8.html",
			h3: "What is the advantage of Chinaundefineds development of AI?",
			para: "On the evening of May 6, at the 'Digital economy Minjiang Night talk'at the second Digital China …",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208dbe83c3a1e3af9fa0c.png",
			imgAlt: "新闻9",
			url: "#",
			h3: "Good news came from my country, the field of AI:China achieved..",
			para: "For a long time, the United States has been the dominant parent in artificial intelligence technology, but over the years…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208dbe83c3a1e3af9fa0e.png",
			imgAlt: "新闻10",
			url: "#",
			h3: "Why is Chinese AI so strong? When the United States came to China,",
			para: "American media have noticed that the rapid development of Chinaundefineds AI industry has given…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208e2e83c3a1e3af9ff14.png",
			imgAlt: "新闻11",
			url: "#",
			h3: "The Intelligent Development of a City: the Industrial Alliance of AI",
			para: "Artificial intelligence is advancing faster and faster. Recently, it began to advocate building…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208e2e83c3a1e3af9ff16.png",
			imgAlt: "新闻12",
			url: "#",
			h3: "The Ministry of Industry and Information Technology has released a list of 24 AI enterprises, ",
			para: "The China Internet Association and the Information Center of the Ministry of Industry and Information Technology jointly…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208e2e83c3a1e3af9ff0d.png",
			imgAlt: "新闻13",
			url: "#",
			h3: "Shanghai has more than 1000 core AI enterprises initially built into a leading area",
			para: "Shanghai has been built as one of the leading areas in the development of artificial intelligence in China…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208e2e83c3a1e3af9ff10.png",
			imgAlt: "新闻14",
			url: "#",
			h3: "Xiang Jun: the Development of AI in China has innate advantages",
			para: "t present, AI is booming all over the world. As a strategic technology to lead the future…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208e2e83c3a1e3af9ff12.png",
			imgAlt: "新闻15",
			url: "#",
			h3: "Wang Zhigang: Chinaundefineds AI development in the worldundefineds first square",
			para: "Wang Zhigang said that the development of artificial intelligence in China is in the first square in the world…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208f2e83c3a1e3afa07da.png",
			imgAlt: "新闻16",
			url: "#",
			h3: "The most challenging Top AI Competition is held in the United States-Ke Zhiyun won the second place",
			para: "In the DAVIS2019 unsupervised multi-objective video segmentation challenge…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208f2e83c3a1e3afa07dc.png",
			imgAlt: "新闻17",
			url: "#",
			h3: "Huawei publishes White Paper on Smart vehicle solution HiCar Ecology！",
			para: "During the 2019 Huawei developer Conference (HDC 2019), Huawei officially issued the HUAWEI HiCar Ecological White Pape…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208f2e83c3a1e3afa07df.png",
			imgAlt: "新闻18",
			url: "#",
			h3: "Tan Jianrong: Product digital twinning and automotive intelligent manufacturing...",
			para: "At this Intelligent Automobile Summit Forum, Tan Jianrong, academician of the Chinese…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208f2e83c3a1e3afa07e1.png",
			imgAlt: "新闻19",
			url: "#",
			h3: "Xu Xiaogang: intelligent Cooperative Robot is a New Node",
			para: "The future of collaboration robot will focus on the upgrading and transformation of load proportion and flexibility…",
			time: "2019-07-27"
		},
		{
			imgSrc: "https://pic.downk.cc/item/5e7208f2e83c3a1e3afa07d7.png",
			imgAlt: "新闻20",
			url: "#",
			h3: "AI is coming! The first consumption of Industrial Robots in China",
			para: "Last year, the main business revenue of Chinaundefineds robot and intelligent manufacturing industry reached 286.296 billion yuan…",
			time: "2019-07-27"
		}]
	}
});

// 翻页模块
var curPage = null;
var pageBtns = document.querySelector(".other-page").querySelectorAll(".btn");
var newsItems = document.querySelectorAll(".news-item");
// 初始化第一页显示
for(var i = 7; i < newsItems.length; i++) {
	newsItems[i].style.display = "none";
}
// 选页
for(var i = 1; i < pageBtns.length - 1; i++) {
	pageBtns[i].index = i;
	pageBtns[i].addEventListener("click", function() {
		curPage = this.index - 1;
		render();
	}); 
}
// 翻页
pageBtns[0].addEventListener("click", function() { // 上一页
	if(curPage > 0) {
		curPage--;
	} else {
		alert("已经是第一页啦");
	}
	render();
});
pageBtns[pageBtns.length - 1].addEventListener("click", function() { // 下一页
	if(curPage < pageBtns.length - 3) {
		curPage++;
	} else {
		alert("已经是最后一页啦");
	}
	render();
});

// 按钮变色
for(var i = 1; i < pageBtns.length - 1; i++) {
	pageBtns[i].addEventListener("click", function() {
		for(var j = 0; j < pageBtns.length; j++) {
			pageBtns[j].style.backgroundColor = "#a5bbd4";
		}
		this.style.backgroundColor = "#546ad9";
	}); 
}

// 渲染内容
function render() {
	for(var i = 0; i < newsItems.length; i++) {
		newsItems[i].style.display = "none";
	}
	for(var j = curPage * 7; j < (curPage+1) * 7 && j < newsItems.length; j++) {
		newsItems[j].style.display = "block";
	}
	// 改变按钮颜色
	for(var j = 0; j < pageBtns.length; j++) {
		pageBtns[j].style.backgroundColor = "#a5bbd4";
	}
	pageBtns[curPage + 1].style.backgroundColor = "#546ad9";
}
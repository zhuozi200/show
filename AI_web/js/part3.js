var aLink = document.querySelectorAll(".panel-title");
			
//绘制背景长条
var collapsePart = document.querySelector(".collapsePart")
var blueBar = document.querySelector(".blue_bar");
//获取整个collapsePart的高度(延时获取，等待元素展开后获取高度)
setTimeout(function() {
	var curHeight = collapsePart.offsetHeight;
	//给长条初始化高度
	blueBar.style.height = curHeight + "px";
}, 80);

//延时添加transition属性, 防止初始加载时也有过渡动画
setTimeout(function() {
	blueBar.style.transition = "height 0.5s";  
}, 1000);

//响应式改变长条高度
window.addEventListener("resize", function() {
	curHeight = collapsePart.offsetHeight;
	blueBar.style.height = curHeight + "px";
});

//以下代码为改变三角形图标方向
//1.添加展开开关(布尔值),初始化所有开关  2.给各项赋予下标值
for (var i = 0; i < aLink.length; i++) {
	aLink[i].children[0].index = i;
	aLink[i].children[0].swit = false;
}
//初始第一个为开
aLink[0].children[0].swit = true;

//添加开关切换事件
for (var i = 0; i < aLink.length; i++) {
	aLink[i].children[0].addEventListener("click", function() {
		var temp = this.swit; //临时保存当前选项开关状态
		//所有开关归位为关,注意此时当前选项也被归位了
		for (var j = 0; j < aLink.length; j++) {
			aLink[j].children[0].swit = false;
		}
		this.swit = temp; //将当前选项归位前的状态重新赋予当前选项

		//正式执行本次开关行为
		this.swit = !this.swit;
		//改变三角形图标状态
		for (var j = 0; j < aLink.length; j++) {
			// console.log("下标为" + aLink[j].children[0].index + "的选项此时的开关值是：" + aLink[j].children[0].swit);
			if (aLink[j].children[0].swit == true) {
				aLink[j].children[0].children[0].className = "glyphicon glyphicon-triangle-bottom";
			} else {
				aLink[j].children[0].children[0].className = "glyphicon glyphicon-triangle-right";
			}
		}
	})
}

//控制小圆点的显示
var circle = document.querySelectorAll(".circle");
for (var i = 0; i < aLink.length; i++) {
	aLink[i].children[0].addEventListener("click", function() {
		for (var j = 0; j < aLink.length; j++) {
			circle[j].classList.remove("on");
		}
		this.nextElementSibling.classList.add("on");
	})
}

//footer部分
var footer = document.querySelector("footer");
var copyRight = footer.children[0].children[1];
console.log(window.getComputedStyle(footer.children[0], null));
var curPaddingTop = parseInt(window.getComputedStyle(footer.children[0], null).paddingTop);
var curPaddingBottom = parseInt(window.getComputedStyle(footer.children[0], null).paddingBottom);
window.addEventListener("load", function() {
	console.log(footer.offsetHeight);
	copyRight.style.height = (footer.offsetHeight - curPaddingTop - curPaddingBottom) + "px";
});
console.log(footer.offsetHeight);

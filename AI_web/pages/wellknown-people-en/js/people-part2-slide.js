window.addEventListener("load", function() {
	var dotsArr = document.querySelector(".dots").children;
	var content = document.querySelector(".content");
	var h3Arr = content.querySelectorAll("h3");
	var para = content.querySelector("p");
	var curUrl = content.querySelector("a");
	var slideImgArr = document.querySelectorAll(".img");
	var prevIndex = 0;
	console.log(slideImgArr);
	var infoArr = [
		{name: "Wu Enda：", hitokoto: "AI in my eyes", intro: "because there are already larger companies in the cloud or mobile world, it is difficult to have a new industry in the field now.", url: "pages/page9_en.html"},
		{name: "Liu Qinfen：", hitokoto: "AI is a challenge", intro: "“Computers have long been able to do calculations that ordinary people cannot do, which is computational intelligence. And then itundefineds called perceptual intelligence.”", url: "pages/page10_en.html"},
		{name: "Jia Yanqin：", hitokoto: "My views of AI", intro: "“How will AI get in this direction? As a AI engineer, I personally feel that we should jump out of the shackles of the framework and look for value in a wider range of areas.”", url: "pages/page11_en.html"},
		{name: "Cen Yunji：", hitokoto: "Looking for Point of AI", intro: "“I really didnundefinedt foresee the era of artificial intelligence coming so soon. But there are some things that must be done under the circumstances that no one else is very optimistic about.”", url: "pages/page12_en.html"},
		{name: "Zou Zihua：", hitokoto: "The AI will persoal heroism", intro: "“Technically, neural network is actually a simple mathematical function, and depth neural network is sometimes not the best choice.”", url: "pages/page13_en.html"}
	];
	for(var i = 0; i < dotsArr.length; i++) {
		dotsArr[i].index = i;  //保存数组下标
	}
	for(var i = 0; i < dotsArr.length; i++) {
		dotsArr[i].addEventListener("click", function() {
			for(var j = 0; j < dotsArr.length; j++) {
				dotsArr[j].classList.remove("focus");
			}
			this.classList.add("focus");
			//更换文字
			h3Arr[0].innerHTML = infoArr[this.index].name;
			h3Arr[1].innerHTML = infoArr[this.index].hitokoto;
			para.innerHTML = infoArr[this.index].intro;
			curUrl.href = infoArr[this.index].url;
			
			//让当前图片的z-index最高
			for(var j = 0; j < dotsArr.length; j++) {
				slideImgArr[j].style.zIndex = 1;
			}
			slideImgArr[this.index].style.zIndex = 5;
			
			//更换图片
			slideImgArr[this.index].classList.add("cur-img");
			var that = this;
			setTimeout(function() {
				removeClass(that);
			}, 500);
		});
	}
	function removeClass(obj) {
		console.log("本次点击的数组下标是：",obj.index)
		console.log("上一次点击的数组下标是：", prevIndex)
		console.log(obj)
		if(prevIndex != obj.index) {
			slideImgArr[prevIndex].classList.remove("cur-img");
		}
		prevIndex = obj.index;
	}
});
window.addEventListener("load", function() {
	var aLi = document.getElementsByClassName("focus")[0].children;
	for(var i = 0; i < aLi.length; i++) {
		aLi[i].index = i;
		aLi[i].addEventListener("click", function() {
			for(var j = 0; j < aLi.length; j++) {
				aLi[j].classList.remove("active");
			}
			this.classList.add("active");
		});
	}
	// 焦点自动轮播
	var i = 1;
	setInterval(function() {
		for(var j = 0; j < aLi.length; j++) {
			aLi[j].classList.remove("active");
		}
		aLi[i].classList.add("active");
		i++;
		if(i >= aLi.length) {
			i = 0;
		}
	}, 4000);
});

// 数据
var vm = new Vue({
	el: '#vueRender',
	data: {
		message: [
			// 发展趋势data
			{
				title: "中国人工智能产业发展新趋势",
				content: "目前，人工智能逐渐从技术走向应用，取得了飞速发展。但是，现阶段的人工智能仍是以特定应用领域为主的弱人工智能。据显示，大部分技术还需要５年以上的时间才能成熟。因此，在人工智能领域还有很多创新点将会持续涌现，包括技术的突破、新的应用场景等，也将伴随着一批创新创业型公司不断成长，并成为某一领域的龙头企业。",
				url: "pages/page1.html"
			},
			{
				title: "中国人工智能的未来到底通向何方？",
				content: "2019 第四届全球人工智能与机器人峰会（CCF-GAIR 2019）于深圳正式召开。峰会由中国计算机学会（CCF）主办，雷锋网、香港中文大学（深圳）承办，深圳市人工智能与机器人研究院协办，得到了深圳市政府的大力指导，是国内人工智能和机器人学术界、工业界及投资界三大领域的顶级交流博览盛会，旨在打造国内人工智能领域极具实力的跨界交流合作平台。",
				url: "pages/page2.html"
			},
			{
				title: "人工智能技术布局最多的国家，中国",
				content: "“中国和美国是AI专利的主要布局区域，中国已经成为被人工智能技术布局最多的国家。”7月6日，在成都举行的工程科技颠覆性技术论坛上，中国工程院院士吕跃广在《信息和电子工程领域颠覆性技术发展初探》报告中，分析中美专利占比、国际专利、两国相互关注程度及专利关注热点等指标后表示。",
				url: "pages/page3.html"
			}
		],
		curTitle: '',
		curContent: '',
		curUrl: '',
		curIndex: 1
	},
	methods: {
		item(e) {
			var curIndex = e.target.index;
			this.typewriterTitle(this.message[curIndex].title);
			this.curContent = this.message[curIndex].content;
			this.curUrl = this.message[curIndex].url;
		},
		typewriterTitle(str) {
			var that = this;
			var i = 0;
			var interval = setInterval(function() {
				curStr = str.substring(0, i);
				that.curTitle = curStr;
				i++;
				if(i > str.length) {
					clearInterval(interval);
				}
			}, 80);
		}
	},
	mounted() {
		this.curTitle = this.message[0].title;
		this.curContent = this.message[0].content;
		this.curUrl = this.message[0].url;
		this.typewriterTitle(this.curTitle);
		var that = this;
		setInterval(function() {
			console.log(that.curIndex);
			that.curTitle = that.message[that.curIndex].title;
			that.curContent = that.message[that.curIndex].content;
			that.curUrl = that.message[that.curIndex].url;
			that.typewriterTitle(that.curTitle);
			that.curIndex++;
			if(that.curIndex >= that.message.length) {
				that.curIndex = 0;
			}
		}, 4000);
	}
});

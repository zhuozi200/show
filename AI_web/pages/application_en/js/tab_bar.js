window.addEventListener("load", function() {
	var aLi = document.getElementsByClassName("tab-bar")[0].children[0].children;
	for(var i = 0; i < aLi.length; i++) {
		aLi[i].index = i;
		aLi[i].addEventListener("click", function() {
			for(var j = 0; j < aLi.length; j++) {
				aLi[j].classList.remove("active");
			}
			this.classList.add("active");
		});
	}
});

// 数据
var vm = new Vue({
	el: '#vueRender',
	data: {
		curIntro: '',
		curBlock: null,
		message: {
			intro: [
				"In the traditional agricultural production activities, irrigation, fertilization and medication depend on manual estimation, which is accomplished by experience and feeling. With the rapid development of artificial intelligence and other fields, intelligent agriculture in China has also begun to rise, which reflects a series of business logic that collects data from the Internet of Things, big data analysis, and artificial intelligence to better solve productivity. Intelligent agriculture is the strategic engine to promote the integration of urban and rural development.",
				"With the rapid development of artificial intelligence technology in the past few years, the commercial application of artificial intelligence has become the focus of attention. Technological giants have laid out vertical industry applications in succession. Startups need to find the right entry point and cultivate industry solutions to build moats. Artificial intelligence will flourish in finance, transportation, retail and manufacturing industries, effectively solving the pain points of these industries. ",
				"Education is a human and intellectual intensive industry. Overdependence on teachers'human resources is the root of the problem in the education industry. For learning with clear objectives such as education, the emergence of AI technology can be said to fundamentally reduce people's dependence, improve teaching efficiency, help teachers teach students in accordance with their aptitude, and make students'learning more effective.",
				"In recent years, the integration of artificial intelligence technology (AI) and medical and health fields has been deepening. The main application scenarios of AI in medical field include voice input medical record, medical image, assistant diagnosis, drug research and development, medical robot and intelligent analysis of big data of personal health, etc."
			],
			order: [
				"https://pic.downk.cc/item/5e72ebd2e83c3a1e3a587c47.png", 
				"https://pic.downk.cc/item/5e72ebd2e83c3a1e3a587c4c.png", 
				"https://pic.downk.cc/item/5e72ebd2e83c3a1e3a587c55.png",
			],
			blocks: [
				// 农业领域
				[
					{
						img: "https://pic.downk.cc/item/5e721596e83c3a1e3a0333bc.png",
						alt: "农业图1",
						h1: "Intelligent monitoring system",
						h2: "——Let AI Understand Agriculture",
						p: "With this system, agriculture can complete the monitoring of soil including fertility, acidity, alkalinity, microorganisms and other values in the field or greenhouse, and the environment including light intensity, climate temperature, wind speed and other data. Besides planting (mainly greenhouse), aquaculture and animal husbandry can also be monitored."
					},
					{
						img: "https://pic.downk.cc/item/5e721596e83c3a1e3a0333be.png",
						alt: "农业图2",
						h1: "Identification system",
						h2: "——Real-time data closely related to crop growth",
						p: "Integrating artificial intelligence with agricultural production decision-making technology, the computer can simulate human brain neural network, then think like human beings, understand agriculture like agricultural experts, and finally let artificial intelligence serve the agricultural field. "
					},
					{
						img: "https://pic.downk.cc/item/5e721596e83c3a1e3a0333c0.png",
						alt: "农业图3",
						h1: "Agricultural Robot",
						h2: "———Traditional farms will move to \"unmanned farms\"",
						p: "Fujian's first artificial intelligence agricultural robot began its round-the-clock inspection on demonstration farms in Israel, China, on July 17.Agricultural robots can carry out automatic inspection, fixed-point acquisition, automatic turning, automatic return, automatic charging, and can automatically avoid or bypass obstacles."
					}
				],
				// 商业领域
				[
					{
						img: "https://pic.downk.cc/item/5e721596e83c3a1e3a0333ba.png",
						alt: "商业图1",
						h1: "Smart Face Brushing Payment",
						h2: "——More convenient, more efficient and safer",
						p: "Face-brushing payment combines with intelligent recognition technology, so that users can no longer rely on mobile phones and other hardware devices, to help consumers save the trouble of cash change, mobile phone code scanning, card input password and so on, and truly realize the \"integration\" of people and wallets or bank cards. "
					},
					{
						img: "https://pic.downk.cc/item/5e721599e83c3a1e3a0336b8.png",
						alt: "商业图2",
						h1: "Financial Assistant",
						h2: "——Providing optimal training and predictive efficiency",
						p: "Based on Shangtang Deep Learning Platform, in the operation scenario of wealth management, it provides tools including precise marketing, user behavior prediction and so on, realizes the fine pre-investment management in an all-round way, installs intelligent engines for business systems, and improves the efficiency of business personnel."
					},
					{
						img: "https://pic.downk.cc/item/5e721599e83c3a1e3a0336be.png",
						alt: "商业图3",
						h1: "SenseGo Intelligent Business",
						h2: "——Intelligent Solution for Retail Industry",
						p: "SenseGo is mainly based on computer vision technology. Through AI technology, Through AI technology, the offline consumption scenario is digitalized to visually reflect the relationship among people, goods and markets, and to provide intelligent guidance for the refined operation and precise marketing of retail enterprises.  "
					}
				],
				// 教育领域
				[
					{
						img: "https://pic.downk.cc/item/5e721599e83c3a1e3a0336c0.png",
						alt: "教育图1",
						h1: "Smart Campus",
						h2: "——a safe, stable, environment friendly, energy-saving Campus",
						p: "Intelligent campus refers to the integrative environment of campus work, learning and life based on the Internet of Things. This integrative environment takes various application service systems as carriers, and fully integrates teaching, scientific research, management and campus life. "
					},
					{
						img: "https://pic.downk.cc/item/5e721599e83c3a1e3a0336c4.png",
						alt: "教育图2",
						h1: "Polaris AI Assisted Instruction",
						h2: "——to Help Schools Reduce Burden and Increase Efficiency",
						p: "Polaris Ai builds a massive database of high-quality resources, and builds online practical functionsfor teachers to help teachers reduce mechanical repetitive work; at the same time, it can help students formulate learning programs scientifically, Accurate pushing + error problem solving analysis\"to achieve efficient personalized adaptive learning."
					},
					{
						img: "https://pic.downk.cc/item/5e721599e83c3a1e3a0336c7.png",
						alt: "教育图3",
						h1: "Intelligent Photo Search",
						h2: "——Realizing Automated Counseling and Question Answering",
						p: "The software such as Xueba Jun and Homework Help all use the intelligent image recognition technology. When students encounter difficulties, they just need to take pictures from their mobile phones and upload them to the cloud. The system can feedback the answers and solutions in one to two seconds. "
					}
				],
				// 医疗领域
				[
					{
						img: "https://pic.downk.cc/item/5e7215a0e83c3a1e3a033d11.png",
						alt: "医疗图1",
						h1: "AI Medical Imaging",
						h2: "——The most mature field of AI medical treatment in China",
						p: "Artificial intelligence medical image is based on the advantages of large data of medical image and the development of image recognition technology. At present, the products are mainly used in disease screening, mainly in the field of cancer and chronic diseases. Through analysis, the report found that most companies have extensive cooperation with hospitals."
					},
					{
						img: "https://pic.downk.cc/item/5e7215a0e83c3a1e3a033d14.png",
						alt: "医疗图2",
						h1: "Health management",
						h2: "——Health management and professional consultation",
						p: "Realize data proofreading and processing, real-time display of health data and health early warning. Based on the auto-correlation analysis of various nutritional factors, diseases and habits, the multi-dimensional personal health portraits are presented to doctors, health managers and customers at the application level, and an intelligent health management scheme is formulated according to the health portrait."
					},
					{
						img: "https://pic.downk.cc/item/5e7215a0e83c3a1e3a033d16.png",
						alt: "医疗图3",
						h1: "Aided Diagnosis System",
						h2: "——Providing all kinds of medical suggestions",
						p: "Wuhan Central Hospital and Beijing Shukun Science and Technology have cooperated to develop an intelligent assistant diagnostic system for coronary CTA images. doctors can automatically reconstruct and interpret coronary CT images in three-dimensional, and then examine and verify the results of the doctors on the basis of automatic output. Recognition and correction."
					}
				]
			]
		}
	},
	methods: {
		item1() {
			this.curIntro = this.message.intro[0];
			this.curBlock = this.message.blocks[0];
		},
		item2() {
			this.curIntro = this.message.intro[1];
			this.curBlock = this.message.blocks[1];
		},
		item3() {
			this.curIntro = this.message.intro[2];
			this.curBlock = this.message.blocks[2];
		},
		item4() {
			this.curIntro = this.message.intro[3];
			this.curBlock = this.message.blocks[3];
		}
	},
	mounted() {
		this.curIntro = this.message.intro[0];
		this.curBlock = this.message.blocks[0];
	}
});
console.log(vm);
